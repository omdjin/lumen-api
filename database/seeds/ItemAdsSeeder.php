<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ItemAdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_ads')->delete();
        $item = app()->make('\App\ItemAds');
        $item->fill([
            'user_id' => 1,
            'category_id' => 1,
            'title' => 'Macbook Pro 13"',
            'price' => 12000000,
            'description' => 'test deskripsi',
            'picture' => 'mbp-2012-late.php',
            'no_hp' => '08134247',
            'city' => 'Yogyakarta',
            'sold' => false,
            'published' => true
        ]);
        $item->save();
    }
}
