<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/*
* Model item ads
*/

class ItemAds extends Model {
    /*
    * Table Database
    */
    protected $table = 'item_ads';

    protected $fillable = [
        'user_id', 'category_id', 'title', 'price', 'description', 'picture', 'no_hp', 'city', 'sold', 'published'
    ];

    public function category() {
        return $this->hasOne('\App\CategoryAds');
    }
    
    public function user() {
        return $this->hasOne('\App\User');
    }
}
