<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/*
* class category model
*/
class CategoryAds extends Model {
    /*table database*/
    protected $table = 'category_ads';

    /*fillable*/
    protected $fillable = ['name'];
}
