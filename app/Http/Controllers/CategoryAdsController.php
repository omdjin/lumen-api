<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryAds;
class CategoryAdsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
    * Get all data from category
    */
    public function index(Request $request) {
        $category = new CategoryAds;
        $res['success'] = true;
        $res['result'] = $category->all();

        return response($res);
    }

    /*
    * Insert database for CategoryAds
    * Url : /category
    */
    public function create(Request $request) {
        $validator = $this->validate($request, [
            'name' => 'required|max:255'
        ]);
        if($validator->fails()) {
            $res['success'] = false;
            $res['result'] = $validator->errors();

            return response($res);
        } else {
            $category = new CategoryAds;
            $category->fill(['name' => $request->name]);
            if($category->save()) {
                $res['success'] = true;
                $res['result'] = 'Success add new category!';
                return response($res);
            } else {
                $res['success'] = false;
                $res['result'] = 'Failed to add database!';
                return response($res);
            }
        }
    }

    /*
    * get one data CategoryAds by id
    * URL : /category/{id}
    */

    public function read(Request $request, $id) {
        $category = CategoryAds::where('id', $id)->first();
        if ($category !== null) {
            $res['success'] = true;
            $res['result'] = $category;

            return response($res);
        } else {
            $res['success'] = false;
            $res['result'] = 'Category not found!';
            return response($res);
        }
    }

    /*
    * Update data category by id
    */
    public function update(Request $request, $id) {
        $validator = $this->validate($request, [
            'name' => 'required|max:255'
        ]);
        if($validator->fails()) {
            $res['success'] = false;
            $res['result'] = $validator->errors();

            return response($res);
        } else {
            $category = CategoryAds::find($id);
            $category->name = $request->input('name');
            if($category->save()) {
                $res['success'] = true;
                $res['result'] = 'Success update : ' . $request->input('name');
                return response($res);
            } else {
                $res['success'] = false;
                $res['result'] = 'Failed to update database!';
                return response($res);
            }
        }
    }

    /*
    * Delete datacategory ads by id
    */
    public function delete(Request $request, $id) {
        $category = CategoryAds::find($id);
        if($category->delete($id)) {
            $res['success'] = true;
            $res['result'] = 'Success delete category!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['result'] = 'Failed to delete category!';
            return response($res);
        }
    }
}
