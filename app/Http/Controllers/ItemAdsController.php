<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemAds;

class ItemAdsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $item_ads = ItemAds::where('published', true)->get();
        if(count($item_ads) !== 0) {
            $res['success'] = true;
            $res['result'] = $item_ads;

            return response($res);
        } else {
            $res['success'] = true;
            $res['result'] = 'No ads have been published.';

            return response($res);
        }
    }

    /* Create */
    public function create(Request $request) {
        $item_ads = new ItemAds;
        $item_ads->fill([
            'user_id' => $request->input('user_id'),
            'category_id' => $request->input('category_id'),
            'title' => $request->input('title'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'picture' => $request->input('picture'),
            'no_hp' => $request->input('no_hp'),
            'city' => $request->input('city'),
            'sold' => false,
            'published' => true
        ]);
        if($item_ads->save()) {
            $res['success'] = true;
            $res['result'] = 'Success add item ads!';

            return response($res);
        }
    }

    /*READ*/
    public function read(Request $request, $id) {
        $item_ads = ItemAds::where('id', $id)->first();
        if($item_ads !== null) {
            $res['success'] = true;
            $res['result'] = $item_ads;

            return response($res);
        } else {
            $res['success'] = false;
            $res['result'] = 'Item ads not found!';

            return response($res);
        }
    }
}
